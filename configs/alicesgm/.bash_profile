# .bash_profile

# Get the aliases and functions
if [ -f ~/.bashrc ]; then
	. ~/.bashrc
fi

# User specific environment and startup programs

PATH=$PATH:$HOME/.local/bin:$HOME/bin

export PATH

proxy()
{
    export X509_USER_PROXY=`ls -t /var/lib/vobox/alice/proxy_repository/*lcgadmin | sed q`
}

tty -s && echo Defining the right proxy... && proxy

