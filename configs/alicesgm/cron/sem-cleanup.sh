#!/bin/sh
# clean up semaphores left behind by the CM

(ipcs | awk '/alicesgm/ { print "-s", $2 }' | sed '$d' | xargs ipcrm) >> /tmp/ipcrm-$$.txt 2>&1

