# DockerVOBOX

Docker build recipe for an ALICE VOBOX.

## What exactly is this?

A Dockerfile that will allow you to quickly build and deploy a preconfigured ALICE VOBOX (**ARC**). Just add your host certificate + key, and you're good to go.

## Requirements

Docker with a MACVLAN bridge network, on a host with CVMFS.

If you've just installed Docker, you can create a new MACVLAN bridge by running the following command:

```shell
$ docker network create -d macvlan --subnet=your.subnet.here/26 \
--gateway=your.gateway.here --ipv6 --subnet your:ipv6:subnet:here::/64 \
--gateway your:ipv6:gateway:here::1 -o parent=yournetworkinterfacehere docknet
```

This will create a new MACVLAN bridge with both IPv4 and IPv6 support, named "docknet". Note that you will **not** be able to ping any containers on this network from the **host** - this is specific to how MACVLAN works.

## Usage

**Setup**:

```shell
#Clone from this repo
$ git clone https://gitlab.cern.ch/jalien/dockervobox_arc.git

#Build image
$ cd dockervobox
$ docker build -t voboximg .

#Run container
$ docker run -d -h voboxhostnamehere -v /cvmfs:/cvmfs --name=voboxnamehere --net=docknet \
--ip=your.ip.for.vobox.here --ip6=your:ipv6:for:vobox:here --privileged voboximg
```
If needed, the flag "**--privileged**" may be omitted, but note that some tools will not work without having elevated permissions.

**Access**:

You can access the VOBOX by either starting a new bash session in the container through Docker:

```shell
$ docker exec -it voboxnamehere /bin/bash
```
Or by using SSH:

```shell
$ ssh root@your.ip.for.vobox.here
```
Password for the root account may be changed from within the Dockerfile before building, or by first accessing the VOBOX through Docker.

**Service management:**

All services are started automatically alongside the container. If needed, you can also manage these manually by using the following syntax:

```shell
$ service yourservice {start|stop|restart|condrestart|try-restart|reload|force-reload|status}
```
As an example, the following command may be used to restart rsyslog:
```shell
$ service rsyslog restart
```

**Deployment:**

1. Install a host certificate.

2. Run these commands:

```shell

$ service alice-box-proxyrenewal start < /dev/null
$ /usr/sbin/edg-mkgridmap --output=/etc/grid-security/grid-mapfile --safe


```

3. Adjust ~/.alien/Environment as needed.


## Precautions

**-** If you plan on running **more than one** VOBOX container on a single host, the system limit for the maximum number of open file descriptors will need to be increased.
The default limit is otherwise bound to be reached at some point, causing the containers and their processes to become irresponsive and terminate.

**-** AutoFS has a known bug where containers may be unable to access CVMFS unless it has previously been accessed, at least once, on the host.
A workaround is to simply disable AutoFS, and do the mounting manually upon rebooting.

