#! /bin/bash

d=`date '+%y%m%d-%H%M%S'`
log=/tmp/init-$d.log

> $log && exec > $log 2>&1 < /dev/null

for i in '' 6
do
    f=/etc/sysconfig/ip"$i"tables
    [ -r $f ] && perl -ne '/^\s*-/ && system "ip'"$i"'tables $_"' < $f
done

#Let's pretend we have an init system
service () { /services/"$@"; }

service dnsmasq start
service rsyslog start
service crond start
service sshd start
service gsisshd start
service autofs start
service alice-box-proxyrenewal start
service fetch-crl-boot start > /tmp/fetch-crl-$$.log 2>&1 < /dev/null &

#Keep the container running
while :; do sleep infinity; done

exit 0
