FROM cern/cc7-base:20201201-1.x86_64

# Add safeguards, repos and packages
# hadolint ignore=DL3033
RUN yum -y install yum-priorities yum-protectbase epel-release && \
    yum -y install http://linuxsoft.cern.ch/wlcg/centos7/x86_64/wlcg-repo-1.0.0-1.el7.noarch.rpm && \
    yum -y install http://repository.egi.eu/sw/production/umd/4/centos7/x86_64/updates/umd-release-4.1.3-1.el7.centos.noarch.rpm && \
    yum -y install ui --skip-broken && \
    yum -y install gsi-openssh-server gsi-openssh-clients wlcg-vobox dnsmasq rsyslog autofs HEP_OSlibs initscripts net-tools apr-util lsof && \
    yum -y install openssh openssh-clients openssh-server && \
    yum clean all

# Setup ssh and add user(s)
# hadolint ignore=DL4006
RUN /usr/bin/ssh-keygen -A && \
    echo "PasswordAuthentication yes" >> /etc/ssh/sshd_config && \
    openssl rand -base64 12 | passwd --stdin root && \
    useradd -p "$(openssl rand -base64 12)" alicesgm


###CONFIG###

COPY ./configs/vobox/etc/vobox-proxy.conf /var/lib/vobox/alice/etc/
COPY ./configs/vobox/start /var/lib/vobox/alice/start
RUN mkdir /var/lib/vobox/alice/stop && \
    chown -R alicesgm.alicesgm /var/lib/vobox/alice/etc && \
    chown -R alicesgm.alicesgm /var/lib/vobox/alice/start && \
    chown -R alicesgm.alicesgm /var/lib/vobox/alice/stop

#alicesgm user
COPY ./configs/alicesgm/ /home/alicesgm
WORKDIR /home/alicesgm
# hadolint ignore=SC2039
RUN mkdir -p bin && ln -s /cvmfs/alice.cern.ch/bin/alien{,d,v} bin && \
    touch /home/alicesgm/enable-sandbox && \
    mkdir -p ALICE/alien-logs && ln -s /dev/null ALICE/alien-logs/access_log && \
    chown -R alicesgm.alicesgm ~alicesgm

#iptables
COPY ./configs/iptables/iptables /etc/sysconfig/
COPY ./configs/iptables/ip6tables /etc/sysconfig/

#voms
COPY ./configs/voms/lcg-voms2.cern.ch.lsc /etc/grid-security/vomsdir/alice/
COPY ./configs/voms/voms2.cern.ch.lsc /etc/grid-security/vomsdir/alice/
COPY ./configs/voms/alice-lcg-voms2.cern.ch /etc/vomses/
COPY ./configs/voms/alice-voms2.cern.ch /etc/vomses/

#gsisshd
RUN rm /etc/gsissh/sshd_config
COPY ./configs/gsisshd/sshd_config /etc/gsissh/

#rsyslog
RUN rm /etc/rsyslog.conf
COPY ./configs/rsyslog/rsyslog.conf /etc/rsyslog.conf

#myproxy
RUN echo "export MYPROXY_SERVER=myproxy.cern.ch" >> /etc/profile.d/grid-env.sh

#cron
COPY ./configs/cron/alice-box-proxyrenewal /etc/cron.d/
COPY ./configs/cron/edg-mkgridmap /etc/cron.d/
COPY ./configs/cron/fetch-crl /etc/cron.d/
COPY ./configs/cron/edg-mkgridmap.conf /etc/
COPY ./configs/cron/grid-mapfile-local /etc/
COPY ./configs/cron/alicesgm /var/spool/cron/
RUN chown -R alicesgm.alicesgm /var/spool/cron/alicesgm && chmod 600 /var/spool/cron/alicesgm
RUN touch /var/lock/subsys/fetch-crl-cron && chmod 644 /etc/cron.d/[aef]*

#arc
RUN echo 'export ARC_LOCATION=/usr' >> /etc/bashrc


###INIT###

#Add init and service scripts
COPY ./init.sh /init.sh
COPY ./services/* /services/
RUN chmod u+x /init.sh
RUN chmod -R u+x /services
RUN ln -s /services/alice-box-proxyrenewal /etc/init.d/

#Add function to give users sysvinit-like call for service scripts
RUN echo 'service () { /services/"$@"; }' >> /etc/bashrc

#Run init script upon container start
CMD [ "/init.sh" ]
